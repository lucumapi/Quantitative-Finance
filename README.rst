Quantitative Finance
====================

.. code-block:: bash

    $ cd src/chapter_*/
    $ echo create build directory
    $ mkdir build
    $ echo enter to build
    $ cd build
    $ echo execute cmake command
    $ cmake -GNinja ..
    $ echo compiler
    $ ninja


.. code-block:: bash

    $ cd doc/finitedifference/
    $ arara finitedifference


.. code-block:: bash

    $ cd doc/introductiontocpp/
    $ arara introductiontocpp