{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating Robust Classes\n",
    "\n",
    "In chapter three we coded a simple class in C++ using the syntax that we had introduced in that chapter. Of course, it was not possible (or even diserable) to discuss all possible 'syntax avenues' because doing so would be confusing. In this chapter we wish to create more robust and efficient classes and we realise this goal by using some of the syntax that C++ offers. In particular, we address a number of issues that have to do with data and object security, such as:\n",
    "\n",
    "Issue 1: Ensuring that objects and their data are created in a safe way.\n",
    "\n",
    "Issue 2: Accesing and using objects in a safe way; avoiding side-effects.\n",
    "\n",
    "Issue 3: Working with object references rather than copies of objects.\n",
    "\n",
    "Issue 4: Optimization: static objects and static member data.\n",
    "\n",
    "This is quite a lot of territory to cover and the results in this chapter will be used again and again throughout this book. Thus, this chapter is a vital link to future chapters.\n",
    "\n",
    "The most important topics are:\n",
    "* Passing parameters to functions by value or by reference.\n",
    "* Function overloading: ability to define several functions having the same name.\n",
    "* More on constructors.\n",
    "* Not all functions need be member functions: non-member functions.\n",
    "* The `const` keyword and its consequences for C++ applications.\n",
    "\n",
    "After having understood these topics and having implemented them in simple classes the reader will have reached a level of expertise approaching yellow belt. We discuss these topics not only because they are supported in C++ but because they help us become good C++ developers. They also promote the reliability and efficiency of our code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Call by reference and call by value\n",
    "In C++ one can create functions taking zero or more arguments in their parameter list. To this end, we need discuss in what forms these arguments are created and used in a function. We take a simple example to motivate what we mean. Let us consider a function that calculates the larger of two numbers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Max(double x, double y)\n",
    "{\n",
    "    if (x > y)\n",
    "        return x;\n",
    "    return y;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a very simple function of course and in this case we say that the inputs parameters `x` and `y` are used in a *call-by-value* manner; this means that copies of these variables are made on the stack when the function is called:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Maxvalue is 1\n"
     ]
    }
   ],
   "source": [
    "#include <iostream>\n",
    "{\n",
    "    double d1 = 1.0;\n",
    "    double d2 = -34536.00;\n",
    "    \n",
    "    // Copies of d1 and d2 offered to the function Max()\n",
    "    double result = Max(d1, d2);\n",
    "    std::cout << \"Maxvalue is \" << result << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case we work with copies of `d1` and `d2` in the body of `Max()` and not `d1` and `d2` themselves. This process is taken care of automatically and you do not have to worry about this as programmer.\n",
    "\n",
    "The call-by-value technique is also applicable, not only to built-in-types as we have just seen but also to class instances (objects). This means that objects (even `big` ones) will be copied if they are used in this call-by-value way. Let us take an example of a class having an embedded fixed-size array as member data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "class SampleClass\n",
    "{\n",
    "public: // For convenience only\n",
    "    // This data created at compile time\n",
    "    double contents[1000];\n",
    "\n",
    "public:\n",
    "    SampleClass(double d)\n",
    "    {\n",
    "        for (int i = 0; i < 1000; i++)\n",
    "        {\n",
    "            contents[i] = d;\n",
    "        }\n",
    "    }\n",
    "\n",
    "    virtual ~SampleClass()\n",
    "    {\n",
    "        std::cout << \"SampleClass instance being deleted\\n\";\n",
    "    }\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Sum(SampleClass myClass)\n",
    "{\n",
    "    double result = myClass.contents[0];\n",
    "    for (int i = 1; i < 1000; i++)\n",
    "    {\n",
    "        result += myClass.contents[i];\n",
    "    }\n",
    "    return result;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SampleClass instance being deleted\n",
      "SampleClass instance being deleted\n"
     ]
    }
   ],
   "source": [
    "{\n",
    "    SampleClass sc(1.0);\n",
    "    double sum = Sum(sc);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Sum2(SampleClass& myClass)\n",
    "{\n",
    "    double result = myClass.contents[0];\n",
    "    for (int i = 1; i < 1000; i++)\n",
    "    {\n",
    "        result += myClass.contents[i];\n",
    "    }\n",
    "    return result;\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case we work directly with the address of the input parameter and not a copy of it. We are, in fact working with the 'live' object and it is in fact possible to modify it in the body of the function that is being used in. For example, after having called the function `Sum2()` we could assign all of its values to zero! This is a side-effect and we shall see how to resolve this problem in the next section.\n",
    "\n",
    "Some remarks on this section are:\n",
    "* You can use call-by-value or call-by-reference for any type or class but in general I use the former for built-in types while I use the latter for objects and class instances.\n",
    "* I try to avoid using pointers as input parameters to functions and we shall discuss why in a later chapter. By this statement we mean a function prototype having the following signature:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Sum2(SampleClass* myClass);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the style reminiscent of bygone days and we advise against its use. We use modern C++ syntax and in this sense it is a 'better C'.\n",
    "* When using call-by-reference it is only necessary to declare an object once as an address while in a main program you can use it as a 'normal' object (that is, without having to use `&`).\n",
    "* An introduction to references, the stack and other topics relevant to the current context can be found in Chapter 22 and 23."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Constant objects everywhere\n",
    "\n",
    "One of the disadvantages of using references in argument lists is that the function is able to modify the input arguments. What we would ideally like to use is the address of an object while at the same time not be able to change the object in any way. To this end, we define the object to be a constant reference by introducing a new keyword as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Sum3(const SampleClass& myClass)\n",
    "{\n",
    "    return 2.0;// N.B. not possible to modify myClass\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The body of this function can no longer modify the input argument and thus we avoid any side effects; the client code that delivered the object to the function can rest assured that the object will not be modified. In fact, if you try to modify the object you will receive a compiler error.\n",
    "\n",
    "The above conclusions are valid for member functions as well as global functions and we shall give some examples in the coming section. But we first wish to discuss another aspect of 'constantness'."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read only (const) member functions\n",
    "After having creating an object (by calling a constructor of its class) we would like to access its data by defining public member functions. To this end, we wish either to (a) give a copy of the data or (b) a reference to the data itself. We discuss the first case here and to this end we look at an example of a class that models two-dimensional points in Cartesian space. Having createad a point we would like to access its x and y coordinates in a read-only manner as it were:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "// Point.hpp\n",
      "//\n",
      "// Header file for Points in two dimensions. A given Point has 3 coordinates\n",
      "// for compatibility with other systems. However, it is not possible to\n",
      "// influence the third coordinate and furthermore, the delivered functionality\n",
      "// is typically two-dimensional.\n",
      "//\n",
      "// (C) Copyright Datasim BV 1995 - 2005\n",
      "\n",
      "#ifndef Point_HPP\n",
      "#define Point_HPP\n",
      "\n",
      "#include <iostream>\n",
      "\n",
      "class Shape\n",
      "{\n",
      "};\n",
      "\n",
      "class Point : public Shape\n",
      "{\n",
      "private:\n",
      "\tdouble x;\t// X coordinate\n",
      "\tdouble y;\t// Y coordinate\n",
      "\n",
      "\tvoid init(double dx, double dy);\n",
      "\n",
      "public:\n",
      "\t// Constructors\n",
      "\tPoint();\t\t\t\t\t\t\t\t// Default constructor\n",
      "\tPoint(double xval, double yval);\t\t// Initialize with x and y value\n",
      "\tPoint(const Point& pt);\t\t\t\t\t// Copy constructor\n",
      "\t~Point();\t\t\t\t\t\t\t\t// Destructor\n",
      "\n",
      "\t// Accessing functions\n",
      "\tdouble X() const;\t\t\t\t\t// The x-coordinate\n",
      "\tdouble Y() const;\t\t\t\t\t// The y-coordinate\n",
      "\n",
      "\t// Modifiers\n",
      "\tvoid X(double NewX);\t\t\t\t\n",
      "\tvoid Y(double NewY);\n",
      "\n",
      "\t// Arithmetic functions\n",
      "\tPoint add(const Point& p) const;\t\t// Return current + p\n",
      "\tPoint subtract(const Point& p) const;\t// Return current - p\n",
      "\tPoint scale(const Point& pt) const;\t\t// Return current * p\n",
      "\tPoint MidPoint(const Point& pt) const;\t\t// Point midway\n",
      "\n",
      "\n",
      "\t// Copy\n",
      "\tPoint& copy(const Point& p);\t\t\t// Copy p in current\n",
      "\n",
      "};\n",
      "\n",
      "\tstd::ostream& operator << (std::ostream& os, const Point& p);\n",
      "\n",
      "#endif // Point_HXX\n",
      "\n"
     ]
    }
   ],
   "source": [
    "!cat Point.hpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "// Point.cpp\n",
      "//\n",
      "// Header file for Points in two dimensions. A given Point has 3 coordinates\n",
      "// for compatibility with other systems. However, it is not possible to\n",
      "// influence the third coordinate and furthermore, the delivered functionality\n",
      "// is typically two-dimensional.\n",
      "//\n",
      "// Last Modifications:\n",
      "// chap04 AM Kick off\n",
      "// chap05 AM Changed using const and reference\n",
      "// chap05 AM Added copy constructor\n",
      "// 2005-5-20 DD modified POINT -> Point\n",
      "//\n",
      "// (C) Copyright Datasim BV 1995 - 2006\n",
      "\n",
      "#include \"Point.hpp\"\n",
      "\n",
      "// Private functions\n",
      "void Point::init(double dx, double dy)\n",
      "{\n",
      "\tx = dx;\n",
      "\ty = dy;\n",
      "}\n",
      "\n",
      "Point::Point()\n",
      "{// Default constructor\n",
      "\tinit(0.0, 0.0);\n",
      "}\n",
      "\n",
      "Point::Point(double newx, double newy)\n",
      "{// Initialize using newx and newy\n",
      "\tinit(newx, newy);\n",
      "}\n",
      "\n",
      "/*\n",
      "Point::Point(double newx, double newy) : x(newx), y(newy)\n",
      "{// Initialize using newx and newy\n",
      "\n",
      "\t// init(newx, newy); NOT NEEDED\n",
      "}\n",
      "*/\n",
      "\n",
      "Point::Point(const Point& pt)\n",
      "{// Copy constructor\n",
      "\tx = pt.x;\n",
      "\ty = pt.y;\n",
      "}\n",
      "\n",
      "Point::~Point()\n",
      "{\n",
      "}\n",
      "\n",
      "double Point::X() const\n",
      "{\n",
      "\treturn x;\n",
      "}\n",
      "\n",
      "double Point::Y() const\n",
      "{\n",
      "\treturn y;\n",
      "}\n",
      "\n",
      "// Modifiers\n",
      "void Point::X(double NewX)\n",
      "{\n",
      "\tx = NewX;\n",
      "}\n",
      "\n",
      "void Point::Y(double NewY)\n",
      "{\n",
      "\ty = NewY;\n",
      "}\n",
      "\n",
      "// Arithmetic functions\n",
      "Point Point::add(const Point& p) const\n",
      "{\n",
      "\treturn Point(x + p.x, y + p.y);\n",
      "}\n",
      "\n",
      "Point Point::subtract(const Point& p) const\n",
      "{\n",
      "\treturn Point(x - p.x, y - p.y);\n",
      "}\n",
      "\n",
      "Point Point::scale(const Point& p) const\n",
      "{ // Scale a Point by another Point\n",
      "\treturn Point(x * p.x, y * p.y);\n",
      "}\n",
      "\n",
      "Point Point::MidPoint(const Point& p2) const\n",
      "{ // Scale a Point by another Point\n",
      "\n",
      "\tPoint result((x + p2.x) * 0.5, (y + p2.y) * 0.5);\n",
      "\n",
      "\treturn result;\n",
      "}\n",
      "\n",
      "\n",
      "// Copy\n",
      "Point& Point::copy(const Point& p)\n",
      "{// Copy p in current\n",
      "\tx = p.x;\n",
      "\ty = p.y;\n",
      "\n",
      "\treturn *this;\n",
      "}\n",
      "\n",
      "// Output\n",
      "std::ostream& operator << (std::ostream& os, const Point& p)\n",
      "{ // Output to screen\n",
      "\n",
      "\tos << \"Point: (\" << p.X() << \", \" << p.Y() << \")\" << std::endl;\n",
      "\n",
      "\treturn os;\n",
      "\n",
      "}\n"
     ]
    }
   ],
   "source": [
    "!cat Point.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat TestPoint.cpp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cpp\n",
    "class Point\n",
    "{\n",
    "private:\n",
    "    void init(double xs, double ys);\n",
    "    \n",
    "    // Properties for x- and y-coordinates\n",
    "    double x, y;\n",
    "public:\n",
    "    // Constructors and destructor\n",
    "    Point(); // Default constructor\n",
    "    Point(double xs, double ys); // Construct with coordinates\n",
    "    Point(const Point& source); // Copy constructor\n",
    "    virtual ~Point(); // Destructor\n",
    "    \n",
    "    // Selectors\n",
    "    double X() const; // Return x\n",
    "    double Y() const; // Return y\n",
    "};\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Point\n",
    "{\n",
    "private:\n",
    "    void init(double xs, double ys);\n",
    "\n",
    "    // Properties for x- and y-coordinates\n",
    "    double x, y;\n",
    "public:\n",
    "    // Constructors and destructor\n",
    "    Point(); // Default constructor\n",
    "    Point(double xs, double ys); // Construct with coordinates\n",
    "    Point(const Point& source); // Copy constructor\n",
    "    virtual ~Point(); // Destructor\n",
    "\n",
    "    // Selectors\n",
    "    double X() const; // Return x\n",
    "    double Y() const; // Return y\n",
    "\n",
    "    // Modifiers\n",
    "\tvoid X(double NewX);\n",
    "\tvoid Y(double NewY);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[1minput_line_8:7:1: \u001b[0m\u001b[0;1;31merror: \u001b[0m\u001b[1mfunction definition is not allowed here\u001b[0m\n",
      "{\n",
      "\u001b[0;1;32m^\n",
      "\u001b[0m"
     ]
    },
    {
     "ename": "Interpreter Error",
     "evalue": "",
     "output_type": "error",
     "traceback": [
      "Interpreter Error: "
     ]
    }
   ],
   "source": [
    "void Point::X(double NewX)\n",
    "{\n",
    "    x = NewX;\n",
    "}\n",
    "void Point::Y(double NewY)\n",
    "{\n",
    "    y = NewY;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we see that the functions `X()` and `Y()` are declared as being constant member functions and this means (in this case) that their bodies cannot modify the private member data of the point. This is like an insurance policy because client code can use these functions and be sure that its objects will not be modified. The body of the two selector functions above is given by:\n",
    "```cpp\n",
    "double Point::X() const\n",
    "{// Return x\n",
    "    return x;\n",
    "}\n",
    "\n",
    "double Point::Y() const\n",
    "{// Return y\n",
    "    return y;\n",
    "}\n",
    "```\n",
    "In this case we state that copies of the member data are returned to the client, not the member data itself. This needs to be known and in other cases we may need to define functions that actually give the address of the member data as return types. This is an issue for a later chapter.\n",
    "\n",
    "On the other hand, functions that modify the member data in general cannot be `const` for obvious reasons; they are, by definition functions that change the member data in some way. In this case the function prototypes are:\n",
    "```cpp\n",
    "// Modifiers\n",
    "void X(double NewX); // Set x\n",
    "void Y(double NewY); // Set y\n",
    "```\n",
    "while the body of these functions is given by:\n",
    "```cpp\n",
    "// Modifiers\n",
    "void Point::X(double NewX)\n",
    "{// Set x\n",
    "    x = NewX;\n",
    "}\n",
    "\n",
    "void Point::Y(double NewY)\n",
    "{// Set y\n",
    "    y = NewY;\n",
    "}\n",
    "```\n",
    "Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "{\n",
    "    Point p1(1.0, 3.14);\n",
    "    // Read the coordinate on the Console\n",
    "    std::cout << \"First coordinate: \" << p1.X() << std::endl;\n",
    "    std::cout << \"Second coordinate: \" << p1.Y() << std::endl;\n",
    "}"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++11",
   "language": "C++11",
   "name": "xcpp11"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
