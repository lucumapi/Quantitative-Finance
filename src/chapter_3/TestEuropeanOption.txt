Call option on a stock: 2.13293
Put option on an index: 2.4648
Put option on a future: 1.70118
Call option on a future: 1.70118

** Other pricing examples **

Call option on a currency: 0.0290937
Delta on a put future: -0.356609
Delta on a call future: 0.59462
