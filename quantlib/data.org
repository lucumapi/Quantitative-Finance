Binomial method European put (exact: 4.0733)

N = 256, 512

T	S	K	r	σ
1	5	10	0.12	0.5


| Name  | Phone | Age |
|-------+-------+-----|
| Peter |  1234 |  17 |
| Anna  |  4321 |  25 |

Binomial method European call (exact: 2.1334)

N = 256

T	S	K	r	σ
0.25	60	65	0.08	0.30